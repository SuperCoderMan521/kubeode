## 版本说明
当前我们标注了两个版本
最新的版本是总所周知的k8s_kubeode_20220619 版本，我们重新定义为版本名称为Kubeode_k8sV2.6.19
命名规则为产品名称_k8sV年份.月份.日期
## 版本号
Kubeode_k8sV2.6.19
## 下载地址
k8s_kubeode_20220619.tar
链接：https://cloud.189.cn/t/JRZrmiBFbeUj
(访问码:6cae)
## 发布日期
2022-06-19
## 特性
1.全新版本kubeode, k8s版本升级v1.23.5
2.支持1-多台，多master高可用。
3.全程Tui终端图形化交互式安装。
## 部署命令

```
tar -xvf  k8s_kubeode_20220619.tar && cd  k8s_kubeode_20220619/ && sh install.sh
```
## 视频指导
[Kubeode_k8sV2.6.19版本部署专题视频_西瓜视频](https://www.ixigua.com/7128215671556702723?wid_try=1)

说明：视频清晰度请进入视频平台后调整为720P观看
